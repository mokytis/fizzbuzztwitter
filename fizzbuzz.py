# FizzBuzz solution
# by: lukespademan

def fizzbuzz_generator():
    rules = {
        3: 'fizz',
        5: 'buzz',
        }
    num = 0
    while True:
        num += 1
        output = ""
        for rule in rules:
            if num % rule == 0:  # if the number is divisable by the rule
                output += rules[rule]  # add the rule text to the output
        yield output if output else num

if __name__ == "__main__":
    fb = fizzbuzz_generator()
    for i in range(100000000000):
        print(next(fb))
