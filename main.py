import twitter
from decouple import config
from fizzbuzz import fizzbuzz_generator
import time

api = twitter.Api(
    consumer_key=config('CONSUMER_KEY', ''),
    consumer_secret=config('CONSUMER_SECRET_KEY', ''),
    access_token_key=config('ACCESS_TOKEN_KEY', ''),
    access_token_secret=config('ACCESS_TOKEN_SECRET', '')
)

def id_in_players(i, p):
    for player in p:
        if player.current_id == i:
            return True
    return False

def find_player(i, p):
    for j, player in enumerate(p):
        if player.current_id == i:
            return j
class Player:
    def __init__(self, initial_id):
        self.current_id = initial_id
        self.fb = fizzbuzz_generator()
        self.history = []

players = []
ignore_ids = []

while True:
    posts = api.GetHomeTimeline(200)
    for p in posts:
        print(f"{p.user.screen_name}\t{p.text}")
        if "start fizzbuzz" in p.text.lower():
            print("STARTED")
            if not id_in_players(p.id, players) and not p.id in ignore_ids:
                players.append(Player(p.id))
                next_fb = str(next(players[-1].fb))
                players[-1].history.append(next_fb)
                status = api.PostUpdate(next_fb, in_reply_to_status_id=p.id, auto_populate_reply_metadata=True)
                players[-1].current_id = status.id
                ignore_ids.append(p.id)
        else:
            if id_in_players(p.in_reply_to_status_id, players):
                pid = find_player(p.in_reply_to_status_id, players) # update objects id
                players[pid].current_id = p.id
                if str(next(players[pid].fb)).lower() in p.text.lower():
                    next_fb = str(next(players[pid].fb))
                    players[pid].history.append(next_fb)
                    status = api.PostUpdate(next_fb, in_reply_to_status_id=p.id, auto_populate_reply_metadata=True)
                    players[pid].current_id = status.id
                else:
                    api.PostUpdate("you lost", in_reply_to_status_id=p.id, auto_populate_reply_metadata=True)
                    players.pop(pid)

    time.sleep((60*60)/50)

#print(status.text)
#users = api.GetFriends()
#print([u.id if u.name == '✨✨🏳️\u200d🌈✨ Ben Misell (they/them) ✨🏳️\u200d🌈✨✨' else None for u in users])
#statuses = api.GetUserTimeline(4898627980, exclude_replies=True, count=100)
#print("\n".join([f"{s.text} {s.id}" for s in statuses]))
